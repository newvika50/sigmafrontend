import React from "react";
import { Nav, Navbar, Button } from "react-bootstrap";
import { NavLink, useParams } from "react-router-dom";

function HeaderLogin() {
  const { idUser } = useParams();
  let link = "/lk/" + { idUser };
  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        bg="primary"
        variant="dark"
        style={{ marginBottom: "10px" }}
      >
        <Navbar.Brand>
          <img
            src={require("../images/logo_white.png")}
            width="70%"
            height="70%"
            className="d-inline-block align-top"
            alt="Si"
          />
        </Navbar.Brand>
      </Navbar>
    </>
  );
}

export default HeaderLogin;
