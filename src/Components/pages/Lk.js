import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Header from "../Header_candidate";
import axios from "axios";
import getAuthUser from "../lib/getAuthUser";

export function Lk() {
  const { auth, data } = getAuthUser();
  const [tasks, setTasks] = useState([]);
  const idUser = data.id;

  useEffect(() => {
    axios
      .get("http://localhost:8080/user_task/" + idUser, {
        headers: {
          "Content-Type": "application/json",
          Authorization: auth,
        },
      })
      .then((response) => {
        setTasks(
          response.data.map((task) => ({
            id: task.taskId,
            name: task.taskName
          }))
        );
      });
  }, []);

  const [me, my_data] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:8080/user/" + idUser, {
        headers: {
          "Content-Type": "application/json",
          Authorization: auth,
        },
      })
      .then((response) => {
        my_data({
          username: response.data.username,
          lastName: response.data.lastName,
          firstName: response.data.firstName,
          patronymic: response.data.patronymic,
          phone: response.data.phone,
          email: response.data.email,
          info: response.data.info
        });
      });
  }, []);

  return (
    <>
      <Header />
      <label
        className="d-grid gap-2"
        style={{ margin: "5px", fontSize: "20px" }}
      >
        Фамилия: {me.lastName}
      </label>
      <label
        className="d-grid gap-2"
        style={{ margin: "5px", fontSize: "20px" }}
      >
        Имя: {me.firstName}
      </label>
      <label
        className="d-grid gap-2"
        style={{ margin: "5px", fontSize: "20px" }}
      >
        Отчество: {me.patronymic}
      </label>
      <label
        className="d-grid gap-2"
        style={{ margin: "5px", fontSize: "15px" }}
      >
        username: {me.username}
      </label>
      <label
        className="d-grid gap-2"
        style={{ margin: "5px", fontSize: "15px" }}
      >
        Телефон: {me.phone}
      </label>
      <label
        className="d-grid gap-2"
        style={{ margin: "5px", fontSize: "15px" }}
      >
        email: {me.email}
      </label>
      <label
        className="d-grid gap-2"
        style={{ margin: "5px", fontSize: "15px" }}
      >
        Дополнительная информация: {me.info}
      </label>
      <label
        className="d-grid gap-2"
        style={{ margin: "5px", fontSize: "40px" }}
      >
        Вам назначено задач: {tasks.length}
      </label>
      <div className="d-grid gap-2">
        {tasks.map((task) => (
          <Link key={task.id} to={"/lk/" + task.id}>
            <li> {task.name}</li>
          </Link>
        ))}
      </div>
    </>
  );
}
