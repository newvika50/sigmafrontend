import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import getAuthUser from "../lib/getAuthUser";
import Editor from "../code-editor/Editor";
import HeaderCandidate from "../Header_candidate";
import axios from "axios";
import { Button } from "react-bootstrap";

function CodeEditor() {
  const [java, setJava] = useState(
    `public class Main {
      public static void main(String[] args) {
        System.out.println("Hello, world!");
      }
    } `
  );
  const [js, setJs] = useState(`console.log("Hello, world!")`);
  const [stdin, setStdin] = useState("");
  const [task, setTask] = useState([]);

  const [resultStatus, setResultStatus] = useState();
  const [resultMemory, setResultMemory] = useState(null);
  const [resultTime, setResultTime] = useState(null);
  const [userTaskId, setUserTaskId] = useState();
  const [compileOutput, setCompileOutput] = useState();
  const [status, setStatus] = useState();
  const [runCode, setRunCode] = useState(null);
  const [isPending, setIsPending] = useState(false);
  const [isSending, setIsSending] = useState(false);
  const [stderr, setStderr] = useState();

  const { idUser, idTask } = useParams();
  const { auth, data } = getAuthUser();

  const pending = "Отправка...";

  const handleStdin = (e) => {
    setStdin(e.target.value);
  };

  const getUserTaskId = () =>
    axios
      .get("http://127.0.0.1:8080/user_task/" + data.id + "/" + idTask, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: auth,
        },
      })
      .then((response) => {
        const a = response.data;
        setUserTaskId(a);
      });

  function NewlineText(props) {
    const text = props.text;
    return text.split("\n").map((str) => (
      <span>
        {str}
        <br />
      </span>
    ));
  }

  const runUserCode = (language_id) => {
    setIsPending(true);
    try {
      axios
        .post(
          "http://127.0.0.1:8080/submissions/test",
          {
            code: language_id == 62 ? java : js,
            lang_id: language_id,
            userTask_id: userTaskId,
            stdin: stdin.toString() + "\\n",
          },
          {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: auth,
            },
          }
        )
        .then((response) => {
          setResultStatus(response.data.stdout);
          setResultMemory(response.data.memory);
          setResultTime(response.data.time);
          setCompileOutput(response.data.compile_output);
          setStatus(response.data.status.description);
          setIsPending(false);
          setStderr(response.data.stderr);
        });
    } catch (err) {
      console.log(err);
    }
  };
  const runLastCode = (language_id) => {
    setIsSending(true);
    axios
      .post(
        "http://127.0.0.1:8080/submissions/finalize",
        {
          code: language_id == 62 ? java : js,
          lang_id: language_id,
          userTask_id: userTaskId,
          stdin: stdin,
        },
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: auth,
          },
        }
      )
      .then((response) => {
        setRunCode(response.data);
        setIsSending(false);
      });
  };
  const loadTaskDataByUserId = () => {
    try {
      axios
        .get("http://localhost:8080/task/" + idTask, {
          headers: {
            "Content-Type": "application/json",
            Authorization: auth,
          },
        })
        .then((response) => {
          console.log(response.data);
          setTask({
            name: response.data.name,
            descriprion: response.data.description,
            tests: response.data.tests,
            topicType: response.data.topicType,
            tests: response.data.tests,
            language_id: response.data.topicType == "Java" ? 62 : 63,
          });
          console.log(response.data.topicType == "Java" ? 62 : 63);
        });
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    loadTaskDataByUserId();
    getUserTaskId();
    return () => {
      console.log("component will unmount");
    };
  }, []);

  return (
    <>
      <HeaderCandidate />
      Название: {task.name} <br />
      Описание: {task.descriprion}
      <div className="codeEditor">
        <div className="tab-button-container"></div>
        <div className="editor-container">
          {task.language_id == 63 ? (
            <Editor language="javascript" value={js} setEditorState={setJs} />
          ) : (
            <Editor
              language="text/x-java"
              value={java}
              setEditorState={setJava}
            />
          )}
        </div>
      </div>
      <div style={{ margin: "10px" }}>
        <input
          type="text"
          onChange={handleStdin}
          value={stdin}
          placeholder="Входные данные"
        />{" "}
        <Button
          variant="secondary"
          onClick={() => runUserCode(task.language_id)}
        >
          Запустить код
        </Button>{" "}
        <Button variant="success" onClick={() => runLastCode(task.language_id)}>
          Завершить попытку
        </Button>{" "}
        <br />
        Результат
        {isPending ? (
          <p>Компилируется...</p>
        ) : (
          <div>
            <p>
              <br />
              {resultStatus ? "Ответ: " : ""}
            </p>
            <p>{resultStatus ? <NewlineText text={resultStatus} /> : ""}</p>
            <p>{resultMemory ? "Память: " + resultMemory + " Кб" : ""} </p>
            <p>{resultTime ? "Время: " + resultTime + "мс" : ""} </p>
            <p>{status ? "Статус: " + status : ""}</p>
            <p>{compileOutput ? "Вывод консоли: " + compileOutput : ""}</p>
            <p>{stderr ? stderr : ""}</p>
            {isSending ? (
              <p>Отправка ...</p>
            ) : (
              <div>
                <p>{runCode ? runCode : ""}</p>
              </div>
            )}
          </div>
        )}
      </div>
    </>
  );
}

export default CodeEditor;
