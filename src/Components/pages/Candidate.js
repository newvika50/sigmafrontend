import React, { Component } from "react";
import Header from "../Header";
import getAuthUser from "../lib/getAuthUser";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import axios from "axios";
import { AddCndModal } from "../modals/AddCndModal";
import { EditCndModal } from "../modals/EditCndModal";
import { AssignTaskToUserModal } from "../modals/AssignTaskToUserModal";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPen,
  faTrashCan,
  faPersonCirclePlus,
} from "@fortawesome/free-solid-svg-icons";

import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";

const { SearchBar, ClearSearchButton } = Search;

import { Table, Button, ButtonToolbar } from "react-bootstrap";

const { auth } = getAuthUser();

const ClearButton = (props) => {
  const handleClick = () => {
    props.onSearch("");
  };
  return (
    <Button
      variant="secondary"
      onClick={handleClick}
      style={{
        fontSize: "16px",
        padding: "5px",
        margin: "10px",
        height: "40px",
      }}
    >
      Clear
    </Button>
  );
};


export class Candidate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          dataField: "username",
          text: "Никнейм",
          sort: true,
        },
        {
          dataField: "lastName",
          text: "Фамилия",
          sort: true,
        },
        {
          dataField: "firstName",
          text: "Имя",
          sort: true,
        },
        {
          dataField: "patronymic",
          text: "Отчество",
          sort: true,
        },
        {
          dataField: "email",
          text: "email",
          sort: true,
        },
        {
          dataField: "phone",
          text: "Телефон",
        },
        ,
        {
          dataField: "role",
          text: "Роль",
          sort: true,
        },
        {
          dataField: "info",
          text: "Дополнительная информация",
          sort: true,
        },
        {
          dataField: "settings",
          text: "",
          formatter: this.linkToBtn,
        }
      ],
      isFollow: true,
      cnds: [],
      addModalShow: false,
      editModalShow: false,
      assignTaskToUserShow: false,
    };
    this._isMounted = false;
  }

  linkToBtn = (cell, row, rowIndex, formatExtraData) => {
    return (
      <>
        <button
          onClick={() =>
            this.setState({
              editModalShow: true,
              cnd_id: row.id,
              cnd_fn: row.firstName,
              cnd_ln: row.lastName,
              cnd_p: row.patronymic,
              cnd_email: row.email,
              cnd_info: row.info,
              cnd_phone: row.phone,
              cnd_username: row.username,
            })
          }
        >
          <FontAwesomeIcon icon={faPen} />
        </button>
        <button
          onClick={() => {
            this.deleteCnd(row.id);
          }}
        >
          <FontAwesomeIcon icon={faTrashCan} />
        </button>
        <button
          onClick={() =>
            this.setState({
              assignTaskToUserShow: true,
              cnd_id: row.id,
              cnd_fn: row.firstName,
              cnd_ln: row.lastName,
              cnd_p: row.patronymic,
            })
          }
        >
          <FontAwesomeIcon icon={faPersonCirclePlus} />
        </button>
      </>
    );
  };

  componentDidMount() {
    this._isMounted = true;
    this.refreshList();
  }

  // componentDidUpdate(){
  //     this.refreshList();
  // }

  componentWillUnmount() {
    this._isMounted = false;
  }

  deleteCnd = (cnd_id) => {
    if (window.confirm("Are you sure?")) {
      axios.delete("http://localhost:8080/user/" + cnd_id, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: auth,
        },
      });
    }
  };

  refreshList() {
    try {
      axios
        .get("http://localhost:8080/user", {
          headers: {
            "Content-Type": "application/json",
            Authorization: auth,
          },
        })
        .then((response) => {
          this.setState({ cnds: response.data });
        });
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    const {
      cnds,
      cnd_id,
      cnd_fn,
      cnd_ln,
      cnd_p,
      cnd_info,
      cnd_phone,
      cnd_email,
      cnd_username,
    } = this.state;
    let addModalClose = () => this.setState({ addModalShow: false });
    let editModalClose = () => this.setState({ editModalShow: false });
    let assignTaskToUserClose = () =>
      this.setState({ assignTaskToUserShow: false });

    return (
      <>
        <Header />
        <ButtonToolbar>
          <Button
            variant="primary"
            onClick={() => this.setState({ addModalShow: true })}
          >
            Добавить кандидата
          </Button>
          <AddCndModal show={this.state.addModalShow} onHide={addModalClose} />
        </ButtonToolbar>
        
        <BootstrapTable
          keyField="id"
          data={cnds}
          columns={this.state.columns}
          pagination={paginationFactory({ sizePerPage: 10 })}
        />
        <EditCndModal
                    show={this.state.editModalShow}
                    onHide={editModalClose}
                    cnd_id={cnd_id}
                    cnd_fn={cnd_fn}
                    cnd_ln={cnd_ln}
                    cnd_info={cnd_info}
                    cnd_p={cnd_p}
                    cnd_phone={cnd_phone}
                    cnd_email={cnd_email}
                    cnd_username={cnd_username}
                  />

                  <AssignTaskToUserModal
                    show={this.state.assignTaskToUserShow}
                    onHide={assignTaskToUserClose}
                    cnd_id={cnd_id}
                    cnd_fn={cnd_fn}
                    cnd_ln={cnd_ln}
                    cnd_p={cnd_p}
                  />
      </>
    );
  }
}
