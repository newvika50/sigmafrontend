import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Header from "../Header";
import getAuthUser from "../lib/getAuthUser";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { faTrashCan } from "@fortawesome/free-solid-svg-icons";

import { Table, ListGroup, Button } from "react-bootstrap";
import axios from "axios";

export function AssignedTasks() {
  const { idUser } = useParams();
  const [userTasks, setUserTasks] = useState([]);
  const { auth } = getAuthUser();
  const  columns = [
    {
      dataField: "taskName",
      text: "Задача",
      sort: true,
    },
    {
      dataField: "fio",
      text: "Кандидат",
      sort: true,
      formatter: (cell, row) => {
        return (
          <div>{`${row.lastName} ${row.firstName} ${row.patronymic}`}</div>
        );
      },
    },
    {
      dataField: "status",
      text: "Статус",
      sort: true,
    },
    {
      dataField: "assignDate",
      text: "Дата назначения задачи",
      formatter: (cell, row) => {
        return <div>{new Date(row.assignDate).toLocaleString()}</div>;
      },
    },
    ,
    {
      dataField: "role",
      text: "Время выполнения",
      sort: true,
      formatter: (cell, row) => {
        return <div>{getTimeCoding(row.startDate, row.submitDate)}</div>;
      },
    },
    {
      dataField: "result",
      text: "Результат",
      sort: true,
      formatter: (cell, row) => {
        return <div>{row.result ? row.result.split("\n")[0] : row.result}</div>;
      },
    },
    {
      dataField: "code",
      text: "Код",
      formatter: (cell, row) => {
        return (
          <Link to={"/assignedTasks/" + row.id + "/" + row.taskId}>
            Просмотреть код
          </Link>
        );
      },
    },
    {
      dataField: "settings",
      text: "aa",
      formatter: (cell, row) => {
        return (
          <>
            <button onClick={() => deleteUserTask(row.id)}>
              <FontAwesomeIcon icon={faTrashCan} />
            </button>
          </>
        );
      }
    },
  ];

  const deleteUserTask = (userTask_id) => {
    if (window.confirm("Are you sure?")) {
      axios.delete("http://localhost:8080/user_task/" + userTask_id, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: auth,
        },
      });
    }
  };

  const getTimeCoding = (startDate, submitDate) => {
    if (!startDate) return "Кандидат ещё не начал";
    if (!submitDate) return "Кандидат ещё не отправил финальное решение";
    submitDate = new Date(submitDate);
    startDate = new Date(startDate);
    let hours = Math.floor((submitDate - startDate) / (60 * 60 * 1000));
    let mins = Math.round(((submitDate - startDate) / (60 * 1000))%60);
    let sec = Math.round(((submitDate - startDate) / (60 * 60 * 60* 1000))%3600);
    let res = "";
    if (hours) res += hours + "ч ";
    if (mins) res += mins + "мин ";
    if (sec) res += sec + "сек";
    return res;
  };

  useEffect(() => {

    try {
      axios
        .get("http://localhost:8080/user_task/", {
          headers: {
            "Content-Type": "application/json",
            Authorization: auth,
          },
        })
        .then((response) => {
          console.log(columns)
          setUserTasks( response.data
          );
        });
    } catch (err) {
      console.log(err);
    }
    return () => {
      console.log("component will unmount");
    };
  }, []);

  return (
    <>
      <Header />

      <BootstrapTable
          keyField="id"
          data={userTasks}
          columns={columns}
          pagination={paginationFactory({ sizePerPage: 10 })}
        />
    </>
  );
}
