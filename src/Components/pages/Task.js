import React, { Component } from "react";
import Header from "../Header";
import getAuthUser from "../lib/getAuthUser";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrashCan } from "@fortawesome/free-solid-svg-icons";

import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { Table, Button, ButtonToolbar } from "react-bootstrap";
import { AddTskModal } from "../modals/AddTskModal";
import { EditTskModal } from "../modals/EditTskModal";
import axios from "axios";

const { auth } = getAuthUser();

export class Task extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          dataField: "name",
          text: "Название",
          sort: true,
        },
        {
          dataField: "description",
          text: "Описание",
          sort: true,
        },
        {
          dataField: "topicType",
          text: "ЯП",
          sort: true,
        },
        {
          dataField: "difficulty",
          text: "Сложность",
          sort: true,
        },
        {
          dataField: "tests",
          text: "Входные данные	",
          sort: true,
        },
        {
          dataField: "settings",
          text: "",
          formatter: this.linkToBtn,
        },
      ],
      tasks: [],
      addModalShow: false,
      editModalShow: false,
      snackbaropen: false,
      snackbarmsg: "",
    };
    this._isMounted = false;
  }

  linkToBtn = (cell, row, rowIndex, formatExtraData) => {
    return (
      <>
        <button
          onClick={() =>
            this.setState({
              editModalShow: true,
              tsk_id: row.id,
              tsk_name: row.name,
              tsk_topic: row.topic,
              tsk_description: row.description,
              tsk_difficulty: row.difficulty,
              tsk_tests: row.tests,
            })
          }
        >
          <FontAwesomeIcon icon={faPen} />
        </button>

        <button onClick={() => this.deleteTask(row.id)}>
          <FontAwesomeIcon icon={faTrashCan} />
        </button>
      </>
    );
  };

  componentDidMount() {
    this._isMounted = true;
    this.refreshList();
  }

  // componentDidUpdate() {
  //     this.refreshList();
  // }

  componentWillUnmount() {
    this.refreshList();
  }

  deleteTask = (task_id) => {
    if (window.confirm("Are you sure?")) {
      axios
        .delete("http://localhost:8080/task/" + task_id, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: auth,
          },
        })
        .then(
          () => {
            this.setState({
              snackbaropen: true,
              snackbarmsg: "Задание удалено успешно!",
            });
          },
          (error) => {
            this.setState({ snackbaropen: true, snackbarmsg: "Ошибка :(" });
          }
        );
    }
    this.setState();
  };

  refreshList() {
    try {
      axios
        .get("http://127.0.0.1:8080/task", {
          headers: {
            "Content-Type": "application/json",
            Authorization: auth,
          },
        })
        .then((response) => {
          this.setState({ tasks: response.data });
        });
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    const {
      tasks,
      tsk_id,
      tsk_name,
      tsk_topic,
      tsk_description,
      tsk_difficulty,
      tsk_tests,
    } = this.state;
    let addModalClose = () => this.setState({ addModalShow: false });
    let editModalClose = () => this.setState({ editModalShow: false });

    return (
      <>
        <Header />
        <ButtonToolbar>
          <Button
            variant="primary"
            onClick={() => this.setState({ addModalShow: true })}
          >
            Добавить задачу
          </Button>
          <AddTskModal show={this.state.addModalShow} onHide={addModalClose} />
        </ButtonToolbar>
        

        <BootstrapTable
          keyField="id"
          data={tasks}
          columns={this.state.columns}
          pagination={paginationFactory({ sizePerPage: 10 })}
        />
        <EditTskModal
                    show={this.state.editModalShow}
                    onHide={editModalClose}
                    tsk_id={tsk_id}
                    tsk_name={tsk_name}
                    tsk_topic={tsk_topic}
                    tsk_description={tsk_description}
                    tsk_difficulty={tsk_difficulty}
                    tsk_tests={tsk_tests}
                  />
      </>
    );
  }
}
