import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Header from "../Header";
import getAuthUser from "../lib/getAuthUser";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashCan } from "@fortawesome/free-solid-svg-icons";

import { Table, ListGroup, Button } from "react-bootstrap";
import axios from "axios";

let my_result = "";

export function CandidatesCode() {
  const { idUserTask, idTask } = useParams();
  const [userTasks, setUserTasks] = useState([]);
  const [task, setTask] = useState([]);
  const [result, setResult] = useState([]);
  const [code, setCode] = useState([]);
  const { auth } = getAuthUser();

  useEffect(() => {
    try {
      axios
        .get("http://localhost:8080/task/" + idTask, {
          headers: {
            "Content-Type": "application/json",
            Authorization: auth,
          },
        })
        .then((response) => {
          console.log(response.data);
          setTask({
            name: response.data.name,
            descriprion: response.data.description,
            tests: response.data.tests,
            topicType: response.data.topicType,
            tests: response.data.tests,
            language_id: response.data.topicType == "Java" ? 62 : 63,
          });
          console.log(response.data.topicType == "Java" ? 62 : 63);
          console.log(task.topicType);
        });
    } catch (err) {
      console.log(err);
    }
  }, []);

  useEffect(() => {
    try {
      axios
        .get("http://localhost:8080/user_task/userTaskId:" + idUserTask, {
          headers: {
            "Content-Type": "application/json",
            Authorization: auth,
          },
        })
        .then((response) => {
          setCode(response.data.code);
          setResult(response.data.result);
        });
    } catch (err) {
      console.log(err);
    }
    return () => {
      console.log("component will unmount");
    };
  }, []);

  return (
    <>
      <Header />
      <div
        style={{margin: "10px"}}>
        <strong>Название:</strong> {task.name} <br />
        <strong>Описание: </strong>{task.descriprion}
        <br />
        <strong>Результат выполнения: </strong>
          {/* {my_result = result.toString().replace("\n,", ",")} */}
          {result.toString().replaceAll("\n,", ",").split("\n").map((str) => (
            <span>{str}<br /></span>
          ))}
        <br />
        <strong>
          Код: <br />
        </strong>
        {code.toString().split("\n").map((str) => (
            <span>{str}<br /></span>
          ))}
      </div>
      
    </>
  );
}
