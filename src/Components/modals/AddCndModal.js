import React, { Component } from "react";
import { Modal, Button, Row, Col, Form } from "react-bootstrap";
import getAuthUser from "../lib/getAuthUser";

import { generate } from "@wcj/generate-password";

import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import axios from "axios";
const { auth } = getAuthUser();

export class AddCndModal extends Component {
  constructor(props) {
    super(props);

    this.state = { snackbaropen: false, snackbarmsg: "" };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  createPassword() {
    return generate({ length: 23 });
  }

  createUsername(firstName, lastName, patronymic) {
    return firstName + patronymic.substring(0, 1) + "_" + lastName;
  }

  snackbarClose = (event) => {
    this.setState({ snackbaropen: false });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    axios
      .post(
        "http://localhost:8080/user",
        {
          id: 112,
          firstName: event.target.FirstName.value,
          lastName: event.target.LastName.value,
          patronymic: event.target.Patronymic.value,
          email: event.target.Email.value,
          phone: event.target.Phone.value,
          username: this.createUsername(
            event.target.FirstName.value,
            event.target.LastName.value,
            event.target.Patronymic.value
          ),
          info: event.target.Info.value,
        },
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: auth,
          },
        }
      )
      .then(
        () => {
          this.setState({
            snackbaropen: true,
            snackbarmsg: "Кандидат добавлен успешно!",
          });
        },
        (error) => {
          this.setState({
            snackbaropen: true,
            snackbarmsg: "Ошибка :( \n" + error,
          });
        }
      );

  };

  render() {
    return (
      <div className="container">
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          open={this.state.snackbaropen}
          autoHideDuration={3000}
          onClose={this.snackbarClose}
          message={<span id="message-id">{this.state.snackbarmsg}</span>}
          action={[
            <IconButton
              key="close"
              arial-label="Close"
              color="inherit"
              onClick={this.snackbarClose}
            >
              x
            </IconButton>,
          ]}
        />

        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Добавить кандидата
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Form onSubmit={this.handleSubmit}>
                <Form.Group className="mb-3" controlId="LastName">
                  <Form.Label>Фамилия</Form.Label>
                  <Form.Control
                    type="text"
                    name="LastName"
                    placeholder="Иванов"
                    autoFocus
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="FirstName">
                  <Form.Label>Имя</Form.Label>
                  <Form.Control
                    type="text"
                    name="FirstName"
                    placeholder="Иван"
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Patronymic">
                  <Form.Label>Отчество</Form.Label>
                  <Form.Control
                    type="text"
                    name="Patronymic"
                    placeholder="Иванович"
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Email">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    name="Email"
                    placeholder="name@example.com"
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Phone">
                  <Form.Label>Телефон</Form.Label>
                  <Form.Control
                    type="phone"
                    name="Phone"
                    placeholder="+79008765443"
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Info">
                  <Form.Label>Дополнительно</Form.Label>
                  <Form.Control as="textarea" rows={3} />
                </Form.Group>
                <Button variant="primary" type="submit">
                  {" "}
                  Добавить{" "}
                </Button>
              </Form>
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.props.onHide}>
              Закрыть
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
