import React, { Component } from "react";
import { Modal, Button, Row, Col, Form } from "react-bootstrap";
import getAuthUser from "../lib/getAuthUser";

import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import axios from "axios";

const { auth } = getAuthUser();

export class EditCndModal extends Component {
  constructor(props) {
    super(props);
    this.state = { snackbaropen: false, snackbarmsg: "" };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  snackbarClose = () => {
    this.setState({ snackbaropen: false });
  };

  handleSubmit(event) {
    event.preventDefault();
    axios
      .put(
        "http://localhost:8080/user/" + event.target.CandidateID.value,
        {
          // id: event.target.СandidateID.value,
          firstName: event.target.FirstName.value,
          lastName: event.target.LastName.value,
          patronymic: event.target.Patronymic.value,
          email: event.target.Email.value,
          phone: event.target.Phone.value,
          info: event.target.Info.value,
          username: event.target.Username.value,
        },
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: auth
          },
        }
      )
      .then(
        () => {
          //alert(result);
          this.setState({
            snackbaropen: true,
            snackbarmsg: "Изменено успешно!",
          });
        },
        (error) => {
          //alert('Failed')
          this.setState({ snackbaropen: true, snackbarmsg: error });
        }
      );
  }

  render() {
    return (
      <div className="container">
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          open={this.state.snackbaropen}
          autoHideDuration={3000}
          onClose={this.snackbarClose}
          message={<span id="message-id">{this.state.snackbarmsg}</span>}
          action={[
            <IconButton
              key="close"
              arial-label="Close"
              color="inherit"
              onClick={this.snackbarClose}
            >
              {" "}
              x{" "}
            </IconButton>,
          ]}
        />

        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Редактировать кандидата
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Form onSubmit={this.handleSubmit}>
                <Form.Group className="mb-3" controlId="CandidateID">
                  <Form.Control
                    type="id"
                    name="id"
                    placeholder="id"
                    disabled
                    defaultValue={this.props.cnd_id}
                    required
                    hidden
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Username">
                  <Form.Label>Никнейм</Form.Label>
                  <Form.Control
                    type="username"
                    name="Username"
                    placeholder="ИванИ_Иванов"
                    disabled
                    defaultValue={this.props.cnd_username}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="LastName">
                  <Form.Label>Фамилия</Form.Label>
                  <Form.Control
                    type="text"
                    name="LastName"
                    placeholder="Иванов"
                    defaultValue={this.props.cnd_ln}
                    autoFocus
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="FirstName">
                  <Form.Label>Имя</Form.Label>
                  <Form.Control
                    type="text"
                    name="FirstName"
                    placeholder="Иван"
                    defaultValue={this.props.cnd_fn}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Patronymic">
                  <Form.Label>Отчество</Form.Label>
                  <Form.Control
                    type="text"
                    name="Patronymic"
                    placeholder="Иванович"
                    defaultValue={this.props.cnd_p}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Email">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    name="Email"
                    placeholder="name@example.com"
                    defaultValue={this.props.cnd_email}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Phone">
                  <Form.Label>Телефон</Form.Label>
                  <Form.Control
                    type="phone"
                    name="Phone"
                    placeholder="+79008765443"
                    defaultValue={this.props.cnd_phone}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Info">
                  <Form.Label>Дополнительно</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    defaultValue={this.props.cnd_info}
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Info">
                  <Button variant="primary" type="submit">
                    {" "}
                    Обновить{" "}
                  </Button>
                </Form.Group>
              </Form>
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.props.onHide}>
              Закрыть
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
