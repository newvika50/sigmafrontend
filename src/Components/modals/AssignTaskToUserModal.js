import React, { Component } from "react";
import {
  Modal,
  Button,
  Row,
  Col,
  Form,
  Table,
  ButtonToolbar,
} from "react-bootstrap";

import getAuthUser from "../lib/getAuthUser";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVialCircleCheck, faTimes } from "@fortawesome/free-solid-svg-icons";

const { auth } = getAuthUser();

import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import axios from "axios";

export class AssignTaskToUserModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      snackbaropen: false,
      snackbarmsg: "",
      clicked: false,
    };
    this._isMounted = false;
  }

  snackbarClose = () => {
    this.setState({ snackbaropen: false });
  };

  assignTask = (task_id) => {
    axios
      .post("http://127.0.0.1:8080/user_task",
        {
          taskId: task_id,
          userId: this.props.cnd_id,
          status: "",
          result: "",
          code: "",
        },
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: auth,
          },
        }
      )
      .then(
        () => {
          this.setState({
            snackbaropen: true,
            snackbarmsg: "Задание назначено успешно!",
          });
        },
        (error) => {
          this.setState({ snackbaropen: true, snackbarmsg: "Ошибка :(" });
        }
      );
  };

  componentDidMount() {
    this._isMounted = true;
    this.refreshList();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  refreshList() {
    try {
      axios
        .get("http://127.0.0.1:8080/task", {
          headers: {
            "Content-Type": "application/json",
            Authorization: auth,
          },
        })
        .then((response) => {
          this.setState({ tasks: response.data });
        });
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    const {
      tasks,
      tsk_id,
      tsk_name,
      tsk_topic,
      tsk_description,
      tsk_difficulty,
      tsk_tests,
    } = this.state;
    return (
      <div className="container">
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          open={this.state.snackbaropen}
          autoHideDuration={3000}
          onClose={this.snackbarClose}
          message={<span id="message-id">{this.state.snackbarmsg}</span>}
          action={[
            <IconButton
              key="close"
              arial-label="Close"
              color="inherit"
              onClick={this.snackbarClose}
            >
              {" "}
              x{" "}
            </IconButton>,
          ]}
        />

        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Назначить кандидату {this.props.cnd_fn} {this.props.cnd_ln}{" "}
              {this.props.cnd_p} задания
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Table className="mt-4" striped bordered hover size="sm">
                <thead>
                  <tr>
                    <th>Название</th>
                    <th>ЯП</th>
                    <th>Сложность</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {tasks.map((tsk) => (
                    <tr key={tsk.id}>
                      <td>{tsk.name}</td>
                      <td>{tsk.topicType}</td>
                      <td>{tsk.difficulty}</td>
                      <td>
                        <button onClick={() => this.assignTask(tsk.id)}>
                          <FontAwesomeIcon icon={faVialCircleCheck} />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.props.onHide}>
              Закрыть
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
