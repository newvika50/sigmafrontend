import React, { Component } from "react";
import { Modal, Button, Row, Form } from "react-bootstrap";
import getAuthUser from "../lib/getAuthUser";

import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import axios from "axios";
const { auth } = getAuthUser();

export class AddTskModal extends Component {
  constructor(props) {
    super(props);

    this.state = { snackbaropen: false, snackbarmsg: "" };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  snackbarClose = () => {
    this.setState({ snackbaropen: false });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    axios
      .post(
        "http://localhost:8080/task",
        {
          name: event.target.Name.value,
          topicType: event.target.TopicType.value,
          description: event.target.Description.value,
          difficulty: event.target.Difficulty.value,
          tests: event.target.Tests.value,
        },
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: auth,
          },
        }
      )
      .then(
        () => {
          this.setState({
            snackbaropen: true,
            snackbarmsg: "Задание добавлено успешно!",
          });
        },
        (error) => {
          this.setState({ snackbaropen: true, snackbarmsg: "Ошибка :(" });
        }
      );
  };

  render() {
    return (
      <div className="container">
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          open={this.state.snackbaropen}
          autoHideDuration={3000}
          onClose={this.snackbarClose}
          message={<span id="message-id">{this.state.snackbarmsg}</span>}
          action={[
            <IconButton
              key="close"
              arial-label="Close"
              color="inherit"
              onClick={this.snackbarClose}
            >
              x
            </IconButton>,
          ]}
        />

        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Добавить задачу
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Form onSubmit={this.handleSubmit}>
                <Form.Group className="mb-3" controlId="Name">
                  <Form.Label>Название</Form.Label>
                  <Form.Control
                    type="text"
                    name="Name"
                    placeholder="Задача о рюкзаке"
                    autoFocus
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="TopicType">
                  <Form.Label>Тематика</Form.Label>
                  <Form.Select aria-label="Default select example">
                    <option>Java</option>
                    <option>JS</option>
                  </Form.Select>
                </Form.Group>

                <Form.Group className="mb-3" controlId="Difficulty">
                  <Form.Label>Сложность</Form.Label>
                  <Form.Select aria-label="Default select example">
                    <option>Senior</option>
                    <option>Middle</option>
                    <option>Junior</option>
                  </Form.Select>
                </Form.Group>

                <Form.Group className="mb-3" controlId="Tests">
                  <Form.Label>Тесты</Form.Label>
                  <Form.Control
                    type="text"
                    name="Tests"
                    placeholder="Входные данные"
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="Description">
                  <Form.Label>Описание</Form.Label>
                  <Form.Control as="textarea" rows={3} />
                </Form.Group>
                <Button variant="primary" type="submit">
                  {" "}
                  Добавить{" "}
                </Button>
              </Form>
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.props.onHide}>
              Закрыть
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
