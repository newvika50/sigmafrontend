import React, { useState } from "react";
import "codemirror/lib/codemirror.css";
import "codemirror/theme/dracula.css";
import "codemirror/theme/base16-dark.css";
import "codemirror/theme/eclipse.css";
import "codemirror/theme/idea.css";
import "codemirror/theme/yonce.css";
import "codemirror/theme/moxer.css";

import "codemirror/mode/javascript/javascript";
import "codemirror/mode/clike/clike";

import "codemirror/addon/edit/closebrackets";
import "codemirror/addon/edit/closetag";

import { Controlled as ControlledEditorComponent } from "react-codemirror2";

const Editor = ({ language, value, setEditorState }) => {
  const [theme, setTheme] = useState("yonce");
  const handleChange = (editor, data, value) => {
    setEditorState(value);
  };

  const themeArray = [ "yonce", "idea", "dracula", "base16-dark", "eclipse", "moxer" ];

  return (
    <div className="editor-container">
      <div style={{ marginBottom: "10px" }}>
        <label>Choose a theme: </label>
        <select
          name="theme"
          onChange={(el) => {
            setTheme(el.target.value);
          }}
        >
          {themeArray.map((theme) => (
            <option value={theme}>{theme}</option>
          ))}
        </select>
      </div>
      <ControlledEditorComponent
        onBeforeChange={handleChange}
        value={value}
        className="code-mirror-wrapper"
        options={{
          lineWrapping: true,
          lint: true,
          mode: language,
          lineNumbers: true,
          theme: theme,
          autoCloseTags: true,
          autoCloseBrackets: true,
          matchBrackets: true,
          matchTags: true,
          spellcheck: true
        }}
      />
    </div>
  );
};

export default Editor;
