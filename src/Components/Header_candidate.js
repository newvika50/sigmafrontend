import React from "react";
import { Nav, Navbar, Button } from "react-bootstrap";
import { NavLink, useParams } from "react-router-dom";

function HeaderCandidate() {
  const { idUser } = useParams();
  let link = "/lk/" + { idUser };
  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        bg="primary"
        variant="dark"
        style={{ marginBottom: "10px" }}
      >
        <Navbar.Brand>
          <img
            src={require("../images/logo_white.png")}
            width="70%"
            height="70%"
            className="d-inline-block align-top"
            alt="Si"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="resposive-navbar-nav" />
        <Navbar.Collapse id="resposive-navbar-nav">
          <Nav className="me-auto"></Nav>

          <NavLink to={`/codeEditor/${idUser}`}>
            <Button variant="light" className="me-2">
              Личный кабинет
            </Button>
          </NavLink>
          <NavLink to="/logout">
            <Button variant="light">Выйти</Button>
          </NavLink>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}

export default HeaderCandidate;
