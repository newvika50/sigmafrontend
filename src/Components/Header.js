import React from "react";
import { Nav, Navbar, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";

function Header() {
  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        bg="primary"
        variant="dark"
        style={{ marginBottom: "10px" }}
      >
        <Navbar.Brand>
          <img
            src={require("../images/logo_white.png")}
            width="70%"
            height="70%"
            className="d-inline-block align-top"
            alt="Si"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="resposive-navbar-nav" />
        <Navbar.Collapse id="resposive-navbar-nav">
          <Nav className="me-auto">
            <NavLink
              to="/"
              className={(isActive) =>
                "nav-link" + (!isActive ? " unselected" : "")
              }
            >
              Кандидаты
            </NavLink>
            <NavLink
              to="/tasks"
              className={(isActive) =>
                "nav-link" + (!isActive ? " unselected" : "")
              }
            >
              Задания
            </NavLink>
            <NavLink
              to="/assignedTasks"
              className={(isActive) =>
                "nav-link" + (!isActive ? " unselected" : "")
              }
            >
              Назначенные задания
            </NavLink>
            <NavLink to="/logout">
              <Button variant="light">Выйти</Button>
            </NavLink>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}

export default Header;
