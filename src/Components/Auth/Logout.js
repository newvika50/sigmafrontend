function Logout() {
  localStorage.removeItem("data");
  localStorage.removeItem("auth");

  location.reload();
}

export default Logout;
