import "bootstrap/dist/css/bootstrap.min.css";
import "./main.css";

import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

import { Lk } from "./components/pages/Lk";
import { Candidate } from "./components/pages/Candidate";
import { Task } from "./components/pages/Task";
import { AssignedTasks } from "./components/pages/AssignTasks";
import CodeEditor from "./components/pages/CodeEdittor";
import Auth from "./components/Auth/Auth";
import getAuthUser from "./components/lib/getAuthUser";
import Logout from "./components/Auth/Logout";
import { CandidatesCode } from "./components/pages/CandidatesCode";

const App = () => {
  const { data, permission } = getAuthUser();

  return (
    <BrowserRouter>
      <Routes>
        {data ? (
          !permission ? (
            <>
              <Route path="/lk" element={<Lk />} />
              <Route path="/lk/:idTask" element={<CodeEditor />} />
              <Route path="*" element={<Navigate to="/lk" />} />
              <Route path='/logout' element={<Logout/>} />
            </>
          ) : (
            <>
              <Route path="/candidates" element={<Candidate />} />
              <Route path="/tasks" element={<Task />} />
              <Route path="/assignedTasks" element={<AssignedTasks />} />
              <Route path="/assignedTasks/:idUserTask/:idTask" element={<CandidatesCode />} />
              <Route path="*" element={<Candidate />} />
              <Route path='/logout' element={<Logout/>} />
            </>
          )
        ) : (
          <>
            <Route path="/auth" element={<Auth />} />
            <Route path="*" element={<Navigate to="/auth" />} />
          </>
        )}
      </Routes>
    </BrowserRouter>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
